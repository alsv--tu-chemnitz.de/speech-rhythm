form Settings
    word Sound_file_extension .wav
    comment Press OK to choose a directory of sound files to annotate.

endform

# select directory to read from
directory$ = chooseDirectory$: "Choose a directory with 'sound_file_extension$'
... files to annotate."
@getFiles: directory$, sound_file_extension$

# Create the output file and write the first line.
outputPath$ = directory$+"/speech_rhythm.csv"
writeFileLine: "'outputPath$'", "file,id,sentence_id,type,duration"
# loop through files

sentenceID = 0
intervalID = 0

for i to getFiles.length
    soundfile = Read from file: getFiles.files$ [i]
    @getTextGrid: getFiles.files$ [i]

    selectObject: soundfile, getTextGrid.textgrid

	# Do analysis here
	# Extract the names of the Praat objects
	thisSound$ = selected$("Sound")
	thisTextGrid$ = selected$("TextGrid")
	# Extract the number of intervals in the phoneme tier
	select TextGrid 'thisTextGrid$'
	numberOfPhonemes = Get number of intervals: 3

	# Loop through each interval on the phoneme tier.
	for thisInterval from 1 to numberOfPhonemes
		intervalID = intervalID + 1
		thisIntervalType$ = Get label of interval: 3, thisInterval
		thisIntervalStartTime = Get start point: 3, thisInterval
		thisIntervalEndTime   = Get end point:  3, thisInterval
		thisIntervalDuration = thisIntervalEndTime - thisIntervalStartTime
		thisWordInterval = Get interval at time: 1, thisIntervalStartTime
		thisIntervalWord$ = Get label of interval: 1, thisWordInterval
		searchRegex$ = "\.|\!|\?"
		if endsWith(thisIntervalWord$, ".") or endsWith(thisIntervalWord$, "!") or endsWith(thisIntervalWord$, "?")
			sentenceID = sentenceID + 1
		endif
		if endsWith(thisIntervalType$, "pause")
			sentenceID = sentenceID + 1
		endif
		appendFileLine: "'outputPath$'",
			    ...thisSound$, ",",
			    ...intervalID, ",",
			    ...sentenceID, ",",
			    ...thisIntervalType$, ",",
			    ...thisIntervalDuration
	endif
	endfor
     # This is stupid, but I don't know a better way to remove the formant object
     removeObject: getTextGrid.textgrid
     removeObject: soundfile
endfor



procedure getTextGrid: .soundfile$
    .path$ = replace$: .soundfile$, sound_file_extension$, ".TextGrid", 0
    .textgrid = Read from file: .path$
endproc

procedure getFiles: .dir$, .ext$
    .obj = Create Strings as file list: "files", .dir$ + "/*" + .ext$
    .length = Get number of strings

    for .i to .length
        .fname$ = Get string: .i
        .files$ [.i] = .dir$ + "/" + .fname$

    endfor

    removeObject: .obj

endproc
