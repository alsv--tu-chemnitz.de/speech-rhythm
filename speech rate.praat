# clear debug output of previous runs
clearinfo

form Settings
    word Sound_file_extension .wav
    comment Press OK to choose a directory of sound files to annotate.

endform

# select directory to read from
directory$ = chooseDirectory$: "Choose a directory with 'sound_file_extension$'
... files to annotate."
@getFiles: directory$, sound_file_extension$

# Create the output file and write the first line.
outputPath$ = directory$+"/speech_rate.csv"
writeFileLine: "'outputPath$'", "file,num_phonemes,length,num_speech_phonemes,speech_duration"

# loop through files
for i to getFiles.length
    soundfile = Read from file: getFiles.files$ [i]
    @getTextGrid: getFiles.files$ [i]
    selectObject: soundfile, getTextGrid.textgrid


    # Do analysis here
    # Extract the names of the Praat objects
    thisSound$ = selected$("Sound")
    thisTextGrid$ = selected$("TextGrid")
    # Extract the number of intervals in the phoneme tier
    select TextGrid 'thisTextGrid$'
    numberOfPhonemes = Get number of intervals: 2

    # Create the Formant Object
    select Sound 'thisSound$'
    total_length = Get total duration

    speech_duration = 0
    num_speech_phonemes = 0
    for thisInterval from 1 to numberOfPhonemes
        select TextGrid 'thisTextGrid$'
        thisPhoneme$ = Get label of interval: 2, thisInterval
        #lexical_set$="aɪ|aʊ|eɪ|i|oʊ|u|æ|ɑ|ɔ|ɔɪ|ɛ|ɝ|ɪ|ʊ|ʌ"
	lexical_set$= "AA|AE|AH|AO|AW|AX|AY|EH|ER|EY|IH|IX|IY|OW|OY|UH|UW|UX"
	    if index_regex (thisPhoneme$, "'lexical_set$'")
        #if thisPhoneme$ != ""
            thisPhonemeStartTime = Get start point: 2, thisInterval
	        thisPhonemeEndTime   = Get end point:  2, thisInterval
	        thisPhonemeDuration = thisPhonemeEndTime - thisPhonemeStartTime
            speech_duration += thisPhonemeDuration
            num_speech_phonemes += 1
        endif
    endfor

    # Save to a spreadsheet
	appendFileLine: "'outputPath$'", 
			    ...thisSound$, ",",
			    ...numberOfPhonemes, ",",
			    ...total_length, ",",
                ...num_speech_phonemes, ",",
                ...speech_duration  
     # This is stupid, but I don't know a better way to remove the formant object
     select soundfile
     Remove
     select TextGrid 'thisTextGrid$'
     Remove
endfor

procedure getTextGrid: .soundfile$
    .path$ = replace$: .soundfile$, sound_file_extension$, ".TextGrid", 0
    .textgrid = Read from file: .path$
endproc

procedure getFiles: .dir$, .ext$
    .obj = Create Strings as file list: "files", .dir$ + "/*" + .ext$
    .length = Get number of strings

    for .i to .length
        .fname$ = Get string: .i
        .files$ [.i] = .dir$ + "/" + .fname$

    endfor

    removeObject: .obj
endproc