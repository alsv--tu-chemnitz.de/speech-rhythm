form Settings
    word Sound_file_extension .wav
    comment Press OK to choose a directory of sound files to annotate.

endform

# select directory to read from
directory$ = chooseDirectory$: "Choose a directory with 'sound_file_extension$'
... files to annotate."
@getFiles: directory$, sound_file_extension$

# loop through files
for i to getFiles.length
	soundfile = Read from file: getFiles.files$ [i]
	@getTextGrid: getFiles.files$ [i]
	selectObject: soundfile, getTextGrid.textgrid

    # Do analysis here
	# Extract the names of the Praat objects
    thisSound$ = selected$("Sound")
    thisTextGrid$ = selected$("TextGrid")
    # Extract the number of intervals in the phoneme tier
    select TextGrid 'thisTextGrid$'
    # generate output names
    base_name$ = selected$ ("TextGrid")
    outputPath$ = directory$+"/"+base_name$+".TextGrid"

	numberOfTiers = Get number of tiers
	if numberOfTiers > 2
		Remove tier: 3
	endif

    numberOfPhonemes = Get number of intervals: 2
	# Duplicate phoneme tier
	Duplicate tier: 2, 3, "CV"
    # Loop through each interval on the phoneme tier.
    for thisInterval from 1 to numberOfPhonemes
        # Get the label of the interval
        select TextGrid 'thisTextGrid$'
        thisPhoneme$ = Get label of interval: 3, thisInterval
		previousPhoneme$ = ""
        nextPhoneme$ = ""
		if thisInterval > 1
			previousPhoneme$ = Get label of interval: 3, thisInterval - 1
		endif
		if thisInterval < numberOfPhonemes
			nextPhoneme$ = Get label of interval: 3, thisInterval + 1
		endif
		# only vowels contain stress marks
		lexical_set$ = "0|1|2"
		if index_regex (thisPhoneme$, "'lexical_set$'")
			# this is a vowel
			Set interval text: 3, thisInterval, "V"
		elif startsWith(thisPhoneme$, "spn") = 1
			# unreadble
			Set interval text: 3, thisInterval, "spn"
		elif thisPhoneme$ = ""
		# empty or pause
		Set interval text: 3, thisInterval, "pause"
		else
			# this is a consonant
			Set interval text: 3, thisInterval, "C"
			if thisInterval > 1
				if thisInterval < numberOfPhonemes
					# not first and last phoneme, so we can remove boundaries beteween consonants
					if startsWith(previousPhoneme$, "C") = 1
					# previous phoneme is a consonant, thus we remove the boundary
					selection_start = Get start point: 3, thisInterval
					Remove boundary at time: 3, selection_start
					numberOfPhonemes = numberOfPhonemes - 1
					Set interval text: 3, thisInterval -1 , "C"
					thisInterval = thisInterval -1
					endif
				endif
			endif
		endif
    endfor
    Write to text file... 'outputPath$'
    # This is stupid, but I don't know a better way to remove the formant object
    removeObject: getTextGrid.textgrid
    removeObject: soundfile
endfor



procedure getTextGrid: .soundfile$
	.path$ = replace$: .soundfile$, sound_file_extension$, ".TextGrid", 0
	.textgrid = Read from file: .path$
endproc

procedure getFiles: .dir$, .ext$
	.obj = Create Strings as file list: "files", .dir$ + "/*" + .ext$
	.length = Get number of strings

	for .i to .length
		.fname$ = Get string: .i
		.files$ [.i] = .dir$ + "/" + .fname$
	endfor
	removeObject: .obj
endproc
